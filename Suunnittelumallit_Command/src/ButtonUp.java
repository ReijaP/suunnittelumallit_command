
public class ButtonUp {
	
	private Command command;
	
	public ButtonUp(Command command) {
		this.command = command;
	}
	
	public void Push() {
		command.execute();
	}

}
