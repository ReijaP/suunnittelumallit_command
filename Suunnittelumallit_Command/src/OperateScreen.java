
public class OperateScreen {

	public static void main(String[] args) {
		
		Screen screen = new Screen();
		Command screenUp = new PushUpCommand(screen);
		Command screenDown = new PushDownCommand(screen);
		ButtonUp buttonup = new ButtonUp(screenUp);
		ButtonDown buttondown = new ButtonDown(screenDown);
		
		buttonup.Push();
		buttondown.Push();

	}

}
