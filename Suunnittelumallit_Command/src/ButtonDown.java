
public class ButtonDown {
	
	private Command command;
	
	public ButtonDown(Command command) {
		this.command = command;
	}
	
	public void Push() {
		command.execute();
	}

}
